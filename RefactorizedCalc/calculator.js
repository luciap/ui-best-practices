import { OPERATOR_ERROR, INPUT_ERROR } from "./errors";

const OPERATORS = "+-*/";
const EMPTY_VALUE = 0;
const DEFAULT_NUM = [0];
const ALLOWED_SEPARATOR = ",";

const isString = (list) => typeof list === "string";
const isNumber = (value) => typeof value === "number";

const Calculator = {
  add: (numbers, operator) =>
    eval(
      numbers.reduce((accumulator, current) => {
        return accumulator + operator + current;
      })
    ),

  handleEmptyValues: (entry) => {
    if (entry.length === EMPTY_VALUE) {
      return DEFAULT_NUM;
    }
    return entry;
  },

  executeOperation: (numbers, operator) => {
    try {
      numbers = Calculator.handleEmptyValues(numbers);

      if (isString(numbers)) {
        numbers = Calculator.parseStringToNumbers(numbers);
      }

      Calculator.validateNumbers(numbers);
      Calculator.validateOperator(operator);

      let result = Calculator.add(numbers, operator);
      return result;
    } catch (error) {
      return error
    }
  },

  validateOperator: (operator) => {
    if (!OPERATORS.includes(operator)) throw Error(OPERATOR_ERROR);
  },

  validateNumbers: (input) => {
    if (!input.every((number) => isNumber(number) && !isNaN(number)))
      throw Error(INPUT_ERROR);
  },

  parseStringToNumbers: (string) => {
    Calculator.handleEmptyValues(string);
    try {
      return string.split(ALLOWED_SEPARATOR).map((number) => parseInt(number));
    } catch (error) {
      throw Error(INPUT_ERROR);
    }
  },
};

export default Calculator;
