1. Make sure each piece of code is doing what you expect it to do.
2. The method should be easy to read and understand. 30,34 op nums
3. Meaningful Names lines: 30, 34
4. The methods should be small. The optimal number of parameters of a method is zero, after one and two. lines: 6 runValidationsAndExecuteOperation does a lot, separate responsabilities (executeOperation, add, ...)
5. Methods must do something and return something. handleEmptyValues, pure function
6. Comments
7. Make good abstraction and encapsulation. handleEmptyValues function
8. Don’t comment code that will not be used, remove, it just pollutes the code and leaves no doubt in anyone reading. negatives error function

