import { OPERATOR_ERROR, INPUT_ERROR, NEGATIVES_ERROR } from "./errors";

const OPERATORS = "+-*/";

const Calculator = {
  runValidationsAndExecuteOperation: (numbers, operator) => {
    try {
      // Validations
      if (numbers.length === 0) numbers = [0];

      // 7 -> move inside better encapsulation
      if (typeof numbers === "string") {
        numbers = Calculator.parseStringToNumbers(numbers);
      }

      Calculator.numbs(numbers);
      Calculator.op(operator);

      // Exec
      return eval(
        numbers.reduce((res, num) => {
          return res + operator + num;
        })
      );
    } catch (error) {
      return error;
    }
  },

  op: (operator) => {
    if (!OPERATORS.includes(operator)) throw Error(OPERATOR_ERROR);
  },

  numbs: (numbers) => {
    if (
      !numbers.every((number) => typeof number === "number" && !isNaN(number))
    ) {
      throw Error(INPUT_ERROR);
    }
  },

  parseStringToNumbers: (string) => {
    if (string.length === 0) return [0];

    // Just allow COMMA as a separator
    try {
      return string.split(",").map((number) => parseInt(number));
    } catch (e) {
      throw Error(INPUT_ERROR);
    }
  },
  // Not being used, just as an exmple
  negativesError: (numbers) => {
    if (numbers.includes("-")) {
      throw Error(NEGATIVES_ERROR);
    }
  },
};

export default Calculator;
