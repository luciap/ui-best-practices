export const INPUT_ERROR = 'Input error'
export const OPERATOR_ERROR = 'Operator not found'
export const NEGATIVES_ERROR = 'Negatives not allowed'