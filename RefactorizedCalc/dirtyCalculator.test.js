import Calculator from './dirtyCalculator';
import {OPERATOR_ERROR, INPUT_ERROR} from './errors'

test("Empty string adds 0", () => {
    const result = Calculator.runValidationsAndExecuteOperation("", '+')
    expect(result).toBe(0)
});

test("Adds one number", () => {
    const result = Calculator.runValidationsAndExecuteOperation("1", '+')

    expect(result).toBe(1)
});

test("Adds 2 numbers", () => {
    const result = Calculator.runValidationsAndExecuteOperation("1,2", '+')

    expect(result).toBe(3)
});

test("Adds unknown amount of numbers", () => {
    const result = Calculator.runValidationsAndExecuteOperation("1,2,3", '+')

    expect(result).toBe(6)
});

test("Adds unknown amount of numbers as an array", () => {
    const result = Calculator.runValidationsAndExecuteOperation([3,3,3], '+')

    expect(result).toBe(9)
});

test("Empty array adds 0", () => {
    const result = Calculator.runValidationsAndExecuteOperation([], '+')
    expect(result).toBe(0)
});

test("Array with letters", () => {
    const result = Calculator.runValidationsAndExecuteOperation(['a', 'b'], '+')
    expect(result.message).toBe(INPUT_ERROR)
});

test("String with letters", () => {
    const result = Calculator.runValidationsAndExecuteOperation('1,d,2', '+')
    expect(result.message).toBe(INPUT_ERROR)
});

test("Wrong operator", () => {
    const result = Calculator.runValidationsAndExecuteOperation('1,2,2', '%')
    expect(result.message).toBe(OPERATOR_ERROR)
});